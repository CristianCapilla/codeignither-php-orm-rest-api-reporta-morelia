<?php

	class UsuariosModel extends CI_Model{
    	function get_all(){
    	$response = array();
    	$this->db->select('username')->from('users')->order_by('username');
    	$query=$this->db->get();
        return $query->result_array();
    }



    function get_one($username){

        $response = array();

        $this->db->select('username,email')->from('users')->where('username', $username);

        $query = $this->db->get();

        return $query->result_array();

    }

    function update($user){
        $updatedUser['username'] = $user['newUsername'];
        $updatedUser['email'] = $user['email'];
        $this->db->where('username', $user['oldUsername']);
        $this->db->update('users', $updatedUser);
    }

    function put($user){
        $this->db->insert('users', $user);
    }

    function delete($username){
        $this->db->where('username', $username);
       return $this->db->delete('users');
    }

    function insertBatch($datos){
        $this->db->truncate('users');
        $this->db->insert_batch('users', $datos);
    }





     

    }

?>