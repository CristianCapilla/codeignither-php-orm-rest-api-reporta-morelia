<?php

	class ReportsModel extends CI_Model{
    	function get_all(){
    	$response = array();
    	$this->db->select('*')->from('reports')->order_by('date');
    	$query=$this->db->get();
        return $query->result_array();
    }

    function get_one($id){
        $response = array();
        $this->db->select('*')->from('reports')->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function post($report){
        $this->db->insert('reports', $report);
    }
}

?>