<?php

class Migration_Users extends CI_Migration{
	public function up(){
		$this->down();
		$fields = array(
			'username' => array(
					'type' => 'VARCHAR',
					'constraint' => 20,
					'null' => FALSE
				),
			'email' => array(
					'type' => 'VARCHAR',
					'constraint' => 30,
					'null' => FALSE
				),
			'password' => array(
					'type' => 'VARCHAR',
					'constraint' => 30,
					'null' => FALSE
				)
			);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('username', TRUE);

		$this->dbforge->create_table('usuarios');
	}

	public function down(){
		$this->dbforge->drop_table('usuarios');
	}
}
?>